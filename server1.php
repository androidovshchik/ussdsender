<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: POST');
header('Content-Type: application/xml; charset=utf-8');
$dataPOST = trim(file_get_contents('php://input'));
$xmlData = simplexml_load_string($dataPOST);
// Разного рода дополнительных проверок здесь нет
// приложение рассчитывает на данные респонзы, если будут другие
// они будут игнорироваться
$imei = $xmlData->imei;
switch ($xmlData->type) {
	case "login":
		echo <<<XML
<?xml version='1.0'?>
<response>
<status>success</status>
<message>вы успешно зарегистрировались</message>
<sign>$xmlData->sign</sign>
</response>
XML;
		break;
	case "command":
		$commandId = rand(1, 1000);
    	echo <<<XML
<?xml version='1.0'?>
<response>
<command_id>$commandId</command_id>
<command_type>ussd</command_type>
<command>*100#</command>
<command_wait_incoming_sms>false</command_wait_incoming_sms>
<command_data></command_data>
<sign>$xmlData->sign</sign>
</response>
XML;
		break;
	case "result":
		echo <<<XML
<?xml version='1.0'?>
<response>
<command_id>$xmlData->result->command_id</command_id>
<command_status>accepted</command_status>
<sign>$xmlData->sign</sign>
</response>
XML;
		break;
}