package rf.androidovshchik.ussdsender.services;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;
import timber.log.Timber;

public abstract class BaseCallback implements Callback {

    protected String tag = "";

    @Override
    public void onFailure(Call call, IOException e) {
        Timber.e(e);
        if (handleCallback()) {
            showMessage("Не удалось выполнить запрос");
        }
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        ResponseBody responseBody = response.body();
        if (handleCallback()) {
            if (responseBody != null) {
                parseResponse(responseBody.string());
            } else {
                showMessage("Не удалось обработать респонз");
            }
        }
    }

    private void parseResponse(String xml) {
        Timber.d(xml);
        try {
            XmlPullParser xpp = createXmlParser(xml);
            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    case XmlPullParser.START_TAG:
                        tag = xpp.getName();
                        Timber.d("START_TAG: " + tag);
                        break;
                    case XmlPullParser.TEXT:
                        String text = xpp.getText();
                        if (text.trim().isEmpty()) {
                            break;
                        }
                        Timber.d("TEXT: " + text);
                        onText(text);
                        break;
                    default:
                        break;
                }
                xpp.next();
            }
        } catch (Exception e) {
            Timber.e(e);
            showMessage("Ошибка парсинга респонза");
        }
        onFinish();
    }

    private XmlPullParser createXmlParser(String xml) throws Exception {
        XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
        factory.setNamespaceAware(true);
        XmlPullParser xpp = factory.newPullParser();
        xpp.setInput(new StringReader(xml));
        return xpp;
    }

    public abstract void showMessage(String message);

    public abstract boolean handleCallback();

    public abstract void onText(String text);

    public abstract void onFinish();
}