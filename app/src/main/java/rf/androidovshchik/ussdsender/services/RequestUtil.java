package rf.androidovshchik.ussdsender.services;

import org.simpleframework.xml.Serializer;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import okhttp3.Callback;
import okhttp3.Request;
import okhttp3.RequestBody;
import rf.androidovshchik.ussdsender.USSDSender;
import timber.log.Timber;

public class RequestUtil {

    @SuppressWarnings("all")
    public static String getMd5Hash(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String md5 = number.toString(16);
            while (md5.length() < 32)
                md5 = "0" + md5;
            return md5;
        } catch (NoSuchAlgorithmException e) {
            Timber.e(e);
            return "";
        }
    }

    public static void makeCall(String url, Serializer serializer, Object object, Callback callback) {
        String xml = getXmlFromObject(serializer, object);
        Timber.d(xml);
        RequestBody body = RequestBody.create(USSDSender.SOAP_MEDIA_TYPE, xml);
        Request request = new Request.Builder()
            .url(url)
            .post(body)
            .addHeader("Content-Type", "application/xml")
            .build();
        USSDSender.CLIENT.newCall(request).enqueue(callback);
    }

    private static String getXmlFromObject(Serializer serializer, Object object) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            serializer.write(object, outputStream);
            return outputStream.toString("utf8");
        } catch (Exception e) {
            Timber.e(e);
            return "";
        }
    }
}
