package rf.androidovshchik.ussdsender.services;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.app.Service;
import android.content.Context;
import android.content.Intent;

public class ServiceUtil {

    @SuppressWarnings("all")
    public static boolean isRunning(Context context, Class<? extends Service> serviceClass) {
        ActivityManager manager = (ActivityManager)
                context.getSystemService(Context.ACTIVITY_SERVICE);
        for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static Intent getIntent(Context context, Class<? extends Service> service) {
        return new Intent(context, service);
    }
}
