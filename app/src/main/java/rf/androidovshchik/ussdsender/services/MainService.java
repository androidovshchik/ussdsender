package rf.androidovshchik.ussdsender.services;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Telephony;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.doctoror.rxcursorloader.RxCursorLoader;
import com.squareup.sqlbrite3.BriteDatabase;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Call;
import rf.androidovshchik.ussdsender.BuildConfig;
import rf.androidovshchik.ussdsender.R;
import rf.androidovshchik.ussdsender.USSDUtil;
import rf.androidovshchik.ussdsender.data.Prefs;
import rf.androidovshchik.ussdsender.models.Command;
import rf.androidovshchik.ussdsender.models.Row;
import rf.androidovshchik.ussdsender.models.remote.Data;
import rf.androidovshchik.ussdsender.models.remote.Request;
import rf.androidovshchik.ussdsender.models.remote.RequestResult;
import rf.androidovshchik.ussdsender.models.remote.Result;
import rf.androidovshchik.ussdsender.models.rx.Subscriber;
import timber.log.Timber;

public class MainService extends BaseService {

	private PhoneStateListener phoneStateListener = new PhoneStateListener() {

		@Override
		public void onCallStateChanged(int state, String incomingNumber) {
			switch (state) {
				case TelephonyManager.CALL_STATE_IDLE:
					Timber.d("onCallStateChanged: CALL_STATE_IDLE");
					if (isCallingPhone) {
						Timber.d("Dismissed call");
						isCallingPhone = false;
						PhoneUtil.killPhoneCall(telephonyManager);
						onFinishCommand();
					}
					break;
				case TelephonyManager.CALL_STATE_OFFHOOK:
					Timber.d("onCallStateChanged: CALL_STATE_OFFHOOK");
					break;
				default:
					Timber.d("onCallStateChanged: " + state);
			}
		}
	};

	@SuppressWarnings("all")
	private final RxCursorLoader.Query smsQuery = new RxCursorLoader.Query.Builder()
		.setContentUri(Uri.parse("content://sms"))
		.setSortOrder(Telephony.Sms.DATE + " DESC")
		.create();

	private TelephonyManager telephonyManager;

	private CompositeDisposable smsDisposable = new CompositeDisposable();

	private CompositeDisposable delayCallDisposable = new CompositeDisposable();

	private CompositeDisposable delayUSSDDisposable = new CompositeDisposable();

	private CompositeDisposable delaySMSDisposable = new CompositeDisposable();

	private int limitCall = 10;

	private int limitUSSD = 10;

	private int limitSMS = 10;

	private boolean isProcessing = false;

	private boolean isCallingPhone = false;

	private int counterUSSD = 0;

	private boolean isWaitingForUSSD = false;

	private boolean isWaitingForSMS = false;

	private Command command;

	@Override
	@SuppressWarnings("all")
	public void onCreate() {
		super.onCreate();
		if (!hasConditions()) {
			if (prefs.has(Prefs.WORK)) {
				showMessage("Отсутствуют требуемые условия для работы");
			}
			stopSelf();
			return;
		}
		startForeground(1, "Взаимодействие с сервером", R.drawable.ic_cloud_white_24dp);
		command = new Command();
		telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_CALL_STATE);
	}

	@Override
	@SuppressWarnings("all")
	public int onStartCommand(Intent intent, int flags, int startId) {
		Timber.d("onStartCommand: Main service");
		serviceDisposable.add(Observable.interval(prefs.getInt(Prefs.INTERVAL), TimeUnit.SECONDS)
			.observeOn(Schedulers.io())
			.subscribe((Long value) -> {
				if (!hasConditions()) {
					prefs.printAll();
					if (prefs.has(Prefs.WORK)) {
						showMessage("Отсутствуют требуемые условия для работы");
					}
					if (!USSDUtil.isAccessibilitySettingsOn(getApplicationContext())) {
						showMessage("Не предоставлен доступ к диалоговым окнам");
					}
					stopWork();
					return;
				}
				if (!isProcessing) {
					Timber.d("--------------------");
					isProcessing = true;
					checkCommand();
				}
			}));
		return super.onStartCommand(intent, flags, startId);
	}

	@SuppressWarnings("all")
	private void checkCommand() {
		limitCall = prefs.getInt(Prefs.LIMIT_CALL);
		limitUSSD = prefs.getInt(Prefs.LIMIT_USSD);
		limitSMS = prefs.getInt(Prefs.LIMIT_SMS);
		command.login = prefs.getString(Prefs.LOGIN);
		command.id = Row.NONE;
		command.commandId = Row.NONE;
		command.commandType = null;
		command.command = null;
		command.commandData = "";
		command.answer = "";
		command.ussd = "";
		command.status = null;
		command.commandWiSMS = false;
		command.smsFilter = null;
		command.smsRxFromPhone = null;
		counterUSSD = 0;
        isCallingPhone = false;
		isWaitingForUSSD = false;
		isWaitingForSMS = false;
		Request request = new Request();
		request.type = "command";
		request.login = prefs.getString(Prefs.LOGIN);
		request.password = prefs.getString(Prefs.PASSWORD);
		request.imei = telephonyManager.getDeviceId();
		request.sign = RequestUtil.getMd5Hash(request.login + request.password +
			request.type + request.imei);
		RequestUtil.makeCall(prefs.getString(Prefs.URL), serializer, request, new BaseServiceCallback() {

			@Override
			public void onFailure(Call call, IOException e) {
				super.onFailure(call, e);
				isProcessing = false;
			}

			@Override
			public  void onText(String text) {
				switch (tag) {
					case "command_id":
						try {
							command.commandId = Long.parseLong(text);
						} catch (Exception e) {
							Timber.e(e);
							showMessage("Ошибка парсинга идентификатора команды");
						}
						break;
					case "command_type":
						command.commandType = text;
						break;
					case "command_wait_incoming_sms":
						command.commandWiSMS = Boolean.parseBoolean(text);
						break;
					case "command_wait_sms_time":
						limitSMS = Integer.parseInt(text);
						break;
					case "command_wait_ussd_time":
						limitUSSD = Integer.parseInt(text);
						break;
					case "command_call_duration":
						limitCall = Integer.parseInt(text);
						break;
					case "incomming_sms_filter":
						command.smsFilter = text.replaceAll("[\\+\\-()]", "");
						break;
					case "command":
						command.command = text;
						break;
					case "command_data":
						command.commandData = text;
						break;
				}
			}

			@Override
			public void onFinish() {
				if (!hasConditions()) {
					showMessage("Отсутствуют требуемые условия для работы");
					stopWork();
					return;
				}
				if (command.commandId == Row.NONE || command.commandType == null ||
					command.command == null) {
					Timber.d("Bad parameters. Nothing doing");
					isProcessing = false;
					return;
				}
				if (command.timeReceive == null) {
					command.timeReceive = new Date();
				}
				command.timeReceive.setTime(System.currentTimeMillis());
				write2Db(false, "Inserted command to database");
				handleCommand();
			}
		});
	}

	private void handleCommand() {
		switch (command.commandType) {
			case "phone":
				Timber.d("Calling phone");
				isCallingPhone = true;
				if (!BuildConfig.DEBUG) {
					PhoneUtil.makeCall(getApplicationContext(), command.command);
				}
				delayCallDisposable.add(Observable.just(true).delay(limitCall, TimeUnit.SECONDS)
					.subscribe((Boolean value) -> {
						Timber.d("Finish calling phone");
						if (isCallingPhone) {
							isCallingPhone = false;
							PhoneUtil.killPhoneCall(telephonyManager);
							onFinishCommand();
						}
					}));
				break;
			case "ussd":
				Timber.d("Calling ussd");
				isWaitingForUSSD = true;
				if (!EventBus.getDefault().isRegistered(this)) {
					EventBus.getDefault().register(this);
				}
				PhoneUtil.makeCall(getApplicationContext(), command.command);
				delayUSSDDisposable.add(Observable.just(true).delay(limitUSSD, TimeUnit.SECONDS)
					.subscribe((Boolean value) -> {
						Timber.d("Finish waiting USSD");
						if (isWaitingForUSSD) {
							isWaitingForUSSD = false;
							onFinishCommand();
						}
					}));
				break;
			case "sms":
				Timber.d("Sending sms");
				PhoneUtil.sendSMS(getApplicationContext(), command.command, command.commandData);
				onFinishCommand();
				break;
			default:
				Timber.d("Unknown command type. Nothing doing");
				isProcessing = false;
				break;
		}
	}

	@SuppressWarnings("unused")
	@Subscribe(threadMode = ThreadMode.POSTING)
	public void onUSSDText(String text) {
		counterUSSD++;
		if (counterUSSD != 1) {
			return;
		}
		Timber.d("Passed counterUSSD " + counterUSSD);
		if (isWaitingForUSSD) {
			isWaitingForUSSD = false;
			command.ussd = text;
			Timber.d("ussd answer: " + command.ussd);
			onFinishCommand();
		}
	}

	@SuppressWarnings("all")
	private void write2Db(boolean update, String message) {
		BriteDatabase.Transaction transaction = manager.db.newTransaction();
		try {
			if (update) {
				manager.updateRow(command);
			} else {
				command.id = manager.insertRow(command);
				Timber.d(command.toString());
			}
			Timber.d(message);
			transaction.markSuccessful();
		} finally {
			transaction.close();
		}
	}

	@SuppressWarnings("all")
	private void onFinishCommand() {
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		delayCallDisposable.clear();
		delayUSSDDisposable.clear();
		isWaitingForSMS = command.commandWiSMS;
		if (!isWaitingForSMS) {
			submitResult();
		} else {
			Timber.d("Waiting for sms");
			smsDisposable.add(RxCursorLoader.create(getContentResolver(), smsQuery)
				.subscribeOn(Schedulers.io())
				.subscribeWith(new Subscriber<Cursor>() {

					private boolean hasPickedSMS = false;

					@Override
					public void onNext(Cursor cursor) {
						Timber.d("content://sms " + cursor.getCount());
						if (isWaitingForSMS) {
							try {
							    String text = null;
								while (cursor.moveToNext()) {
									if (cursor.getInt(cursor.getColumnIndexOrThrow(Telephony.Sms.TYPE))
										!= Telephony.Sms.MESSAGE_TYPE_INBOX) {
										continue;
									}
									String phone = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.ADDRESS))
										.replaceAll("[\\+\\-()]", "");
									if (command.smsFilter != null && !command.smsFilter.equalsIgnoreCase(phone)) {
                                        continue;
									} else if (command.smsFilter != null) {
										Timber.d("Phones: %s matches %s", command.smsFilter, phone);
									}
									long smsDate = cursor.getLong(cursor.getColumnIndexOrThrow(Telephony.Sms.DATE));
									if (smsDate < command.timeReceive.getTime()) {
										Timber.w("Last sms time is earlier: %d < %d", smsDate,
											command.timeReceive.getTime());
										if (text == null) {
                                            Timber.d("No sms found after command start");
                                        } else if (hasPickedSMS) {
                                            Timber.d("Has already picked SMS");
                                        } else {
                                            hasPickedSMS = true;
                                            isWaitingForSMS = false;
                                            command.answer = text;
                                            Timber.d("sms answer: " + text + "; from phone " + command.smsRxFromPhone);
                                            submitResult();
                                        }
                                        break;
									}
									command.smsRxFromPhone = phone;
                                    text = cursor.getString(cursor.getColumnIndexOrThrow(Telephony.Sms.BODY));
								}
							} catch (Exception e) {
								Timber.e(e);
							} finally {
								cursor.close();
							}
						} else {
							Timber.w("Not waiting for SMS now");
						}
					}
				}));
			delaySMSDisposable.add(Observable.just(true).delay(limitSMS, TimeUnit.SECONDS)
				.subscribe((Boolean value) -> {
					Timber.d("Finish waiting SMS");
					if (isWaitingForSMS) {
						isWaitingForSMS = false;
						submitResult();
					}
				}));
		}
	}

	@SuppressWarnings("all")
	private void submitResult() {
		smsDisposable.clear();
		delaySMSDisposable.clear();
		command.status = "ok";
		if (!command.ussd.isEmpty()) {
			command.answer += Data.SEPARATOR + command.ussd;
			Timber.d("joined answer: " + command.answer);
		} else {
			Timber.d("(without ussd) answer: " + command.answer);
		}
		if (command.timeSend == null) {
			command.timeSend = new Date();
		}
		command.timeSend.setTime(System.currentTimeMillis());
		write2Db(true, "Updated command before sending result");
		RequestResult request = new RequestResult();
		request.type = "result";
		request.login = prefs.getString(Prefs.LOGIN);
		request.password = prefs.getString(Prefs.PASSWORD);
		request.imei = telephonyManager.getDeviceId();
		request.sign = RequestUtil.getMd5Hash(request.login + request.password +
			request.type + request.imei);
		request.result = new Result(command.commandId, command.answer);
		RequestUtil.makeCall(prefs.getString(Prefs.URL), serializer, request, new BaseServiceCallback() {

			boolean isAccepted = false;

			@Override
			public void onFailure(Call call, IOException e) {
				super.onFailure(call, e);
				submitResult();
			}

			@Override
			public  void onText(String text) {
				if (tag.equals("command_status")) {
					isAccepted = text.trim().toLowerCase().equals("accepted");
				}
			}

			@Override
			public void onFinish() {
				if (isAccepted) {
					if (command.timeComplete == null) {
						command.timeComplete = new Date();
					}
					command.timeComplete.setTime(System.currentTimeMillis());
					write2Db(true, "Updated command after receiving accepted status");
					isProcessing = false;
				} else {
					submitResult();
				}
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (EventBus.getDefault().isRegistered(this)) {
			EventBus.getDefault().unregister(this);
		}
		if (smsDisposable != null) {
			smsDisposable.dispose();
		}
		if (delayCallDisposable != null) {
			delayCallDisposable.dispose();
		}
		if (delayUSSDDisposable != null) {
			delayUSSDDisposable.dispose();
		}
		if (delaySMSDisposable != null) {
			delaySMSDisposable.dispose();
		}
	}
}
