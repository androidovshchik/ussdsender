package rf.androidovshchik.ussdsender.services;

import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;

public class CursorCalls extends CursorLoader {

    public CursorCalls(Context context) {
        super(context);
    }

    @Override
    public Cursor loadInBackground() {
        Uri calls = Uri.parse("content://call_log/calls");
        return getContext().getContentResolver().query(calls, null, null, null, null);
    }
}