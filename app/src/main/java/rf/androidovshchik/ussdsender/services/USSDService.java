package rf.androidovshchik.ussdsender.services;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import io.reactivex.annotations.Nullable;
import rf.androidovshchik.ussdsender.data.Prefs;
import timber.log.Timber;

public class USSDService extends AccessibilityService {

    public static final ArrayList<String> USSD_LOGS = new ArrayList<>();

    private Prefs prefs;

    private String getEventType(AccessibilityEvent event) {
        switch (event.getEventType()) {
            case AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED:
                return "(TYPE_NOTIFICATION_STATE_CHANGED)";
            case AccessibilityEvent.TYPE_VIEW_CLICKED:
                return "(TYPE_VIEW_CLICKED)";
            case AccessibilityEvent.TYPE_VIEW_FOCUSED:
                return "(TYPE_VIEW_FOCUSED)";
            case AccessibilityEvent.TYPE_VIEW_LONG_CLICKED:
                return "(TYPE_VIEW_LONG_CLICKED)";
            case AccessibilityEvent.TYPE_VIEW_SELECTED:
                return "(TYPE_VIEW_SELECTED)";
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                return "(TYPE_WINDOW_STATE_CHANGED)";
            case AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED:
                return "(TYPE_VIEW_TEXT_CHANGED)";
        }
        return null;
    }

    @Override
    @SuppressWarnings("all")
    public void onAccessibilityEvent(AccessibilityEvent event) {
        if (prefs == null) {
            log("Null prefs");
            return;
        }
        if (!prefs.has(Prefs.WORK)) {
            return;
        }
        // USSD text
        String text = null;
        if (prefs.getString(Prefs.FETCH_METHOD).equals("1")) {
            if (event.getText().size() > 0) {
                text = event.getText().get(0).toString();
            }
        }
        String type = getEventType(event);
        String format = "onAccessibilityEvent: [time] %d [type] %d %s [package] %s [class] %s [eventText] %s";
        log(String.format(format, event.getEventTime(), event.getEventType(), type == null ?
            "(Неизвестный тип)" : type, event.getPackageName(), event.getClassName(), "" + text));
        if (type == null) {
            return;
        }
        int eventType = prefs.getInt(Prefs.EVENT_TYPE, AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED);
        String eventClass = prefs.getString(Prefs.EVENT_CLASS);
        String eventPackage = prefs.getString(Prefs.EVENT_PACKAGE);
        if (eventType == event.getEventType() && eventClass.equalsIgnoreCase(event.getClassName().toString()) &&
            eventPackage.equalsIgnoreCase(event.getPackageName().toString())) {
            if (prefs.getString(Prefs.FETCH_METHOD).equals("2") || prefs.getString(Prefs.CLOSE_METHOD).equals("2") &&
                prefs.getBoolean(Prefs.CLOSE_DIALOG) || text == null) {
                AccessibilityNodeInfo source = event.getSource();
                if (source != null) {
                    String fetchedResponse = fetchResponse(source, prefs.getBoolean(Prefs.CLOSE_DIALOG) &&
                        prefs.getString(Prefs.CLOSE_METHOD).equals("2"));
                    log("fetchedResponse [GUI]: " + fetchedResponse);
                    if (fetchedResponse != null && (prefs.getString(Prefs.FETCH_METHOD).equals("2") || text == null)) {
                        text = fetchedResponse;
                    }
                } else {
                    log("AccessibilityNodeInfo source is null");
                }
            }
            if (prefs.getBoolean(Prefs.CLOSE_DIALOG) && prefs.getString(Prefs.CLOSE_METHOD).equals("1")) {
                log("Closed window [GlobalMethod]: " + performGlobalAction(GLOBAL_ACTION_BACK));
            }
            if (text != null) {
                log("Yeah, USSD text: " + text);
                if (EventBus.getDefault().hasSubscriberForEvent(String.class)) {
                    EventBus.getDefault().post(text);
                }
            } else {
                log("Failed to find USSD text");
            }
        }
    }

    @Nullable
    private String fetchResponse(AccessibilityNodeInfo accessibilityNodeInfo, boolean close) {
        String fetchedResponse = null;
        if (accessibilityNodeInfo != null) {
            for (int i = 0; i < accessibilityNodeInfo.getChildCount(); i++) {
                AccessibilityNodeInfo child = accessibilityNodeInfo.getChild(i);
                if (child != null) {
                    if (child.getClassName().equals(ScrollView.class.getName())) {
                        // when response comes as phone then response can be retrieved from subchild
                        log("DialogUI[" + i + "]Child: ScrollView class");
                        for (int j = 0; j < child.getChildCount(); j++) {
                            AccessibilityNodeInfo subChild = child.getChild(j);
                            CharSequence subText = subChild.getText();
                            if (subText == null) {
                                log("DialogUI[" + i + "]SubChild: text is null for " + subChild.getClassName());
                                continue;
                            }
                            if (subChild.getClassName().equals(TextView.class.getName())) {
                                log("DialogUI[" + i + ", " + j + "]SubChild: TextView class");
                                // response of different cases
                                fetchedResponse = subText.toString();
                            } else if (subChild.getClassName().equals(Button.class.getName())) {
                                log("DialogUI[" + i + ", " + j + "]SubChild: Button class");
                                if (close) {
                                    // dismiss dialog by performing action click in different cases
                                    log("Closed window [DialogGUISubChild]: " + subChild.performAction(AccessibilityNodeInfo.ACTION_CLICK));
                                }
                                return fetchedResponse;
                            } else {
                                log("DialogUI[" + i + "]SubChild: missing class " + child.getClassName());
                            }
                        }
                        continue;
                    }
                    CharSequence text = child.getText();
                    if (text == null) {
                        log("DialogUI[" + i + "]Child: text is null for " + child.getClassName());
                        continue;
                    }
                    if (child.getClassName().equals(TextView.class.getName())) {
                        log("DialogUI[" + i + "]Child: TextView class");
                        fetchedResponse = text.toString();
                    } else if (child.getClassName().equals(Button.class.getName())) {
                        log("DialogUI[" + i + "]Child: Button class");
                        if (close) {
                            // dismiss dialog by performing action click in normal cases
                            log("Closed window [DialogGUIChild]: " + child.performAction(AccessibilityNodeInfo.ACTION_CLICK));
                        }
                        return fetchedResponse;
                    } else {
                        log("DialogUI[" + i + "]Child: missing class " + child.getClassName());
                    }
                }
            }
        }
        return fetchedResponse;
    }

    @Override
    public void onInterrupt() {}

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        prefs = new Prefs(getApplicationContext());
        log("onServiceConnected");
        AccessibilityServiceInfo info = new AccessibilityServiceInfo();
        info.flags = AccessibilityServiceInfo.DEFAULT;
        info.eventTypes = AccessibilityEvent.TYPES_ALL_MASK;
        info.feedbackType = AccessibilityServiceInfo.FEEDBACK_ALL_MASK;
        setServiceInfo(info);
    }

    private void log(String log) {
        Timber.d(log);
        USSD_LOGS.add(0, log);
    }
}