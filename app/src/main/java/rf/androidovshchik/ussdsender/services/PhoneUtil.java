package rf.androidovshchik.ussdsender.services;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;

import java.lang.reflect.Method;

import rf.androidovshchik.ussdsender.BuildConfig;
import timber.log.Timber;

@SuppressWarnings("all")
public class PhoneUtil {

    public static boolean killPhoneCall(TelephonyManager telephonyManager) {
        try {
            // Get the getITelephony() method
            Class classTelephony = Class.forName(telephonyManager.getClass().getName());
            Method methodGetITelephony = classTelephony.getDeclaredMethod("getITelephony");
            // Ignore that the method is supposed to be private
            methodGetITelephony.setAccessible(true);
            // Invoke getITelephony() to get the ITelephony interface
            Object telephonyInterface = methodGetITelephony.invoke(telephonyManager);
            // Get the endCall method from ITelephony
            Class telephonyInterfaceClass =
                Class.forName(telephonyInterface.getClass().getName());
            Method methodEndCall = telephonyInterfaceClass.getDeclaredMethod("endCall");
            // Invoke endCall()
            methodEndCall.invoke(telephonyInterface);
        } catch (Exception e) {
            // Many things can go wrong with reflection calls
            Timber.e(e);
            return false;
        }
        return true;
    }

    public static void sendSMS(Context context, String phone, String text) {
        if (!BuildConfig.DEBUG) {
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, new Intent(), 0);
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(phone, null, text, pendingIntent, null);
        }
    }

    public static void makeCall(Context context, String number) {
        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL, toUri(number));
            callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(callIntent);
        } catch (SecurityException e) {
            Timber.e(e);
        }
    }

    public static Uri toUri(String ussd) {
        String uriString = "";
        if (!ussd.startsWith("tel:")) {
            uriString += "tel:";
        }
        for (char c : ussd.toCharArray()) {
            switch (c) {
                case '#':
                    uriString += Uri.encode("#");
                    break;
                default:
                    uriString += c;
                    break;
            }
        }
        return Uri.parse(uriString);
    }
}
