package rf.androidovshchik.ussdsender.services;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.DrawableRes;
import android.support.v4.app.NotificationCompat;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import io.reactivex.disposables.CompositeDisposable;
import pub.devrel.easypermissions.EasyPermissions;
import rf.androidovshchik.ussdsender.MainActivity;
import rf.androidovshchik.ussdsender.R;
import rf.androidovshchik.ussdsender.USSDUtil;
import rf.androidovshchik.ussdsender.data.DbManager;
import rf.androidovshchik.ussdsender.data.Prefs;
import rf.androidovshchik.ussdsender.receivers.AlarmUtil;
import rf.androidovshchik.ussdsender.receivers.ToastTrigger;

public abstract class BaseService extends Service {

	private PowerManager.WakeLock wakeLock;

	protected CompositeDisposable serviceDisposable = new CompositeDisposable();

	protected Serializer serializer = new Persister();

	protected DbManager manager;

	protected Prefs prefs;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	@SuppressWarnings("all")
	public void onCreate() {
		super.onCreate();
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
			getString(R.string.app_name));
		wakeLock.acquire();
		manager = new DbManager(getApplicationContext());
		prefs = new Prefs(getApplicationContext());
		AlarmUtil.recreateAlarm(getApplicationContext());
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_NOT_STICKY;
	}

	@SuppressWarnings("all")
	protected void startForeground(int id, String title, @DrawableRes int image) {
		NotificationManager notificationManager = (NotificationManager)
			getSystemService(Context.NOTIFICATION_SERVICE);
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		NotificationCompat.Builder builder = new NotificationCompat.Builder(getApplicationContext(),
			getString(R.string.app_name))
			.setSmallIcon(image)
			.setContentTitle(title)
			.setContentIntent(PendingIntent.getActivity(getApplicationContext(), 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT))
			.setSound(null);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel(getString(R.string.app_name),
				getString(R.string.app_name), NotificationManager.IMPORTANCE_LOW);
			channel.setSound(null, null);
			notificationManager.createNotificationChannel(channel);
			builder.setChannelId(getString(R.string.app_name));
		}
		startForeground(id, builder.build());
	}

	protected boolean hasConditions() {
		return EasyPermissions.hasPermissions(getApplicationContext(), Manifest.permission.CALL_PHONE,
			Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE) &&
			prefs.has(Prefs.LOGIN) && prefs.has(Prefs.PASSWORD) && prefs.has(Prefs.URL)
			&& prefs.has(Prefs.LIMIT_SMS) && prefs.has(Prefs.LIMIT_CALL) && prefs.has(Prefs.LIMIT_USSD)
		    && prefs.has(Prefs.EVENT_TYPE) && prefs.has(Prefs.EVENT_CLASS) && prefs.has(Prefs.EVENT_PACKAGE)
			&& prefs.has(Prefs.WORK) && USSDUtil.isAccessibilitySettingsOn(getApplicationContext());
	}

	protected void stopWork() {
		stopForeground(true);
		stopSelf();
	}

	public void showMessage(String message) {
		Intent intent = new Intent();
		intent.setAction(getPackageName() + ".TOAST");
		intent.putExtra(ToastTrigger.EXTRA_MESSAGE, message);
		sendBroadcast(intent);
	}

	public abstract class BaseServiceCallback extends BaseCallback {

		@Override
		public void showMessage(String message) {
			BaseService.this.showMessage(message);
		}

		@Override
		public boolean handleCallback() {
			return true;
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		serviceDisposable.dispose();
		wakeLock.release();
	}
}
