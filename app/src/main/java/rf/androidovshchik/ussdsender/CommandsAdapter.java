package rf.androidovshchik.ussdsender;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import rf.androidovshchik.ussdsender.models.Command;

public class CommandsAdapter extends RecyclerView.Adapter<CommandsAdapter.ViewHolder> {

    public ArrayList<Command> commands;

    public CommandsAdapter() {
        commands = new ArrayList<>();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_command,
            parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Command command = commands.get(position);
        holder.id.setText(String.valueOf(command.commandId));
        holder.type.setText(String.valueOf(command.commandType));
        holder.command.setText(String.valueOf(command.command));
    }

    @Override
    public int getItemCount() {
        return commands.size();
    }

    public class ViewHolder extends BaseViewHolder {

        @BindView(R.id.id)
        TextView id;
        @BindView(R.id.type)
        TextView type;
        @BindView(R.id.command)
        TextView command;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @OnClick(R.id.container)
        public void onItem() {
            Intent intent = new Intent(getApplicationContext(), DetailsActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(DetailsActivity.EXTRA_COMMAND, commands.get(getAdapterPosition()));
            getApplicationContext().startActivity(intent);
        }
    }
}
