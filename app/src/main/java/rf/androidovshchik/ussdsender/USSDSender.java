package rf.androidovshchik.ussdsender;

import android.app.Application;
import android.view.accessibility.AccessibilityEvent;

import java.security.SecureRandom;
import java.security.cert.CertificateException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import rf.androidovshchik.ussdsender.data.Prefs;
import timber.log.Timber;

public class USSDSender extends Application {

	public static final MediaType SOAP_MEDIA_TYPE = MediaType.parse("application/xml");

	public static OkHttpClient CLIENT;

	static {
		// Create a trust manager that does not validate certificate chains
		@SuppressWarnings("all")
		final TrustManager[] trustAllCerts = new TrustManager[] {
			new X509TrustManager() {
				@Override
				public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
											   String authType) throws CertificateException {}

				@Override
				public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
											   String authType) throws CertificateException {}

				@Override
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return new java.security.cert.X509Certificate[]{};
				}
			}
		};

		OkHttpClient.Builder builder = new OkHttpClient.Builder();
		try {
			// Install the all-trusting trust manager
			final SSLContext sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts, new SecureRandom());
			// Create an ssl socket factory with our all-trusting manager
			final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
			builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0])
				.hostnameVerifier((String hostname, SSLSession session) -> true);
		} catch (Exception e) {
			Timber.e(e);
		}
		builder.addInterceptor(new HttpLoggingInterceptor()
			.setLevel(HttpLoggingInterceptor.Level.BODY))
			.addNetworkInterceptor((Interceptor.Chain chain) -> {
				Request request = chain.request().newBuilder()
					.addHeader("Connection", "close").build();
				return chain.proceed(request);
			});
		CLIENT = builder.build();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Prefs prefs = new Prefs(getApplicationContext());
		if (!prefs.has(Prefs.INTERVAL)) {
			prefs.putInt(Prefs.INTERVAL, 10);
		}
		if (!prefs.has(Prefs.LIMIT_CALL)) {
			prefs.putInt(Prefs.LIMIT_CALL, 10);
		}
		if (!prefs.has(Prefs.LIMIT_SMS)) {
			prefs.putInt(Prefs.LIMIT_SMS, 10);
		}
		if (!prefs.has(Prefs.LIMIT_USSD)) {
			prefs.putInt(Prefs.LIMIT_USSD, 10);
		}
		if (!prefs.has(Prefs.EVENT_TYPE)) {
			prefs.putInt(Prefs.EVENT_TYPE, AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED);
		}
		if (!prefs.has(Prefs.EVENT_CLASS)) {
			prefs.putString(Prefs.EVENT_CLASS, "android.app.AlertDialog");
		}
		if (!prefs.has(Prefs.EVENT_PACKAGE)) {
			prefs.putString(Prefs.EVENT_PACKAGE, "com.android.phone");
		}
		if (!prefs.has(Prefs.CLOSE_DIALOG)) {
			prefs.putBoolean(Prefs.CLOSE_DIALOG, true);
		}
		if (!prefs.has(Prefs.CLOSE_METHOD)) {
			prefs.putString(Prefs.CLOSE_METHOD, "2");
		}
		if (!prefs.has(Prefs.FETCH_METHOD)) {
			prefs.putString(Prefs.FETCH_METHOD, "2");
		}
		Timber.plant(new Timber.DebugTree());
		prefs.printAll();
	}
}