package rf.androidovshchik.ussdsender;

import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import rf.androidovshchik.ussdsender.models.Command;
import timber.log.Timber;

public class DetailsActivity extends Activity {

    public static final String EXTRA_COMMAND = "command";

    @BindView(R.id.commandId)
    TextView commandId;
    @BindView(R.id.commandType)
    TextView commandType;
    @BindView(R.id.commandWiSMS)
    TextView commandWiSMS;
    @BindView(R.id.command)
    TextView command;
    @BindView(R.id.filter)
    TextView filter;
    @BindView(R.id.from)
    TextView from;
    @BindView(R.id.commandData)
    TextView commandData;
    @BindView(R.id.status)
    TextView status;
    @BindView(R.id.answer)
    TextView answer;

    @Override
    @SuppressWarnings("all")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        setTitle("Подробности");
        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }
        ButterKnife.bind(this);
        Command command = (Command) getIntent().getSerializableExtra(EXTRA_COMMAND);
        Timber.d(command.toString());
        commandId.setText("ID команды: " + command.commandId);
        commandType.setText("Тип команды: " + command.commandType);
        commandWiSMS.setText("Требование ожидания входящего сообщения: " + (command.commandWiSMS ? "есть" : "нет"));
        this.command.setText("Команда: " + command.command);
        filter.setText("Смс фильтр: " + (command.smsFilter != null ? command.smsFilter : "нет"));
        from.setText("Смс от: " + (command.smsRxFromPhone != null ? command.smsRxFromPhone : "неизвестно"));
        commandData.setText("Данные команды: " + command.commandData);
        status.setText("Статус: " + (command.status == null ? "обрабатывается" : "завершено"));
        answer.setText("Входящее сообщение: " + command.answer);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
