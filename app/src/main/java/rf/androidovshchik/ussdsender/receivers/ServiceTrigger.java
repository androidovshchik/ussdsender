package rf.androidovshchik.ussdsender.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import rf.androidovshchik.ussdsender.data.Prefs;
import rf.androidovshchik.ussdsender.services.MainService;
import rf.androidovshchik.ussdsender.services.ServiceUtil;
import timber.log.Timber;

public class ServiceTrigger extends BroadcastReceiver {

	protected CompositeDisposable disposable = new CompositeDisposable();

	private Prefs prefs;

	@Override
	public void onReceive(Context context, Intent intent) {
		Timber.d("ServiceTrigger: received");
		prefs = new Prefs(context);
		prefs.remove(Prefs.WORK);
		waitForServiceStop(context);
	}

	private void waitForServiceStop(Context context) {
		disposable.clear();
		disposable.add(Observable.just(true).delay(20, TimeUnit.SECONDS)
			.subscribe((Boolean value) -> {
				Timber.d("Checking service stop");
				if (!ServiceUtil.isRunning(context, MainService.class)) {
					prefs.putBoolean(Prefs.WORK, true);
					startServiceRightWay(context, ServiceUtil.getIntent(context, MainService.class));
					disposable.dispose();
				} else {
					waitForServiceStop(context);
				}
			}));
	}

	private void startServiceRightWay(Context context, Intent intent) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			context.startForegroundService(intent);
		} else {
			context.startService(intent);
		}
	}
}
