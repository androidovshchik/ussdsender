package rf.androidovshchik.ussdsender.data;

import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.squareup.sqlbrite3.SqlBrite;

import java.util.ArrayList;

import rf.androidovshchik.ussdsender.models.Row;
import timber.log.Timber;

public class DbUtil {

    @Nullable
    public static Cursor handleCursor(@Nullable SqlBrite.Query query) {
        if (query == null) {
            Timber.w("Query is null");
            return null;
        }
        Cursor cursor = query.run();
        if (cursor == null) {
            Timber.w("Cursor is null");
            return null;
        }
        return cursor;
    }

    @NonNull
    public static <T extends Row> ArrayList<T> getRows(Cursor cursor, Class<T> rowClass) throws Exception {
        ArrayList<T> rows = new ArrayList<>();
        if (cursor == null) {
            return rows;
        }
        try {
            while (cursor.moveToNext()) {
                T row = rowClass.newInstance();
                row.parseCursor(cursor);
                rows.add(row);
            }
        } finally {
            cursor.close();
        }
        return rows;
    }
}
