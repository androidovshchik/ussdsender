package rf.androidovshchik.ussdsender.data;

import android.arch.persistence.db.SupportSQLiteOpenHelper;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Configuration;
import android.arch.persistence.db.SupportSQLiteOpenHelper.Factory;
import android.arch.persistence.db.framework.FrameworkSQLiteOpenHelperFactory;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.squareup.sqlbrite3.BriteDatabase;
import com.squareup.sqlbrite3.SqlBrite;

import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.ussdsender.BuildConfig;
import rf.androidovshchik.ussdsender.models.Row;
import timber.log.Timber;

public class DbManager {

    public BriteDatabase db;

    public DbManager(Context context) {
        DbCallback dbCallback = new DbCallback();
        dbCallback.openDatabase(context);
        Configuration configuration = Configuration.builder(context)
            .name(DbCallback.DATABASE_NAME)
            .callback(dbCallback)
            .build();
        Factory factory = new FrameworkSQLiteOpenHelperFactory();
        SupportSQLiteOpenHelper openHelper = factory.create(configuration);
        db = new SqlBrite.Builder()
            .logger(message -> Timber.tag("Db").v(message))
            .build()
            .wrapDatabaseHelper(openHelper, Schedulers.io());
        if (BuildConfig.DEBUG) {
            db.setLoggingEnabled(true);
        }
    }

    public long insertRow(Row row) {
        return db.insert(row.getTable(), SQLiteDatabase.CONFLICT_REPLACE, row.toContentValues());
    }

    public int updateRow(Row row) {
        return db.update(row.getTable(), SQLiteDatabase.CONFLICT_IGNORE, row.toContentValues(),
            "id=?", String.valueOf(row.id));
    }

    public int deleteRow(Row row) {
        return db.delete(row.getTable(), "id=?", String.valueOf(row.id));
    }

    @SuppressWarnings("all")
    public int deleteTable(String table) {
        return db.delete(table, null, null);
    }
}
