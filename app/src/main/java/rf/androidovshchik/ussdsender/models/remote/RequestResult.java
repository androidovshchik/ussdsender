package rf.androidovshchik.ussdsender.models.remote;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "request")
public class RequestResult extends Request {

    @Element
    public Result result;
}
