package rf.androidovshchik.ussdsender.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.Date;

public class Command extends Row implements Serializable {

    public static final String TABLE = "commands";

    public static final String COLUMN_LOGIN = "login";
    public static final String COLUMN_COMMAND_ID = "command_id";
    public static final String COLUMN_COMMAND_TYPE = "command_type";
    public static final String COLUMN_COMMAND_WI_SMS = "command_wait_incoming_sms";
    public static final String COLUMN_COMMAND = "command";
    public static final String COLUMN_COMMAND_DATA = "command_data";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_ANSWER = "answer";
    public static final String COLUMN_TIME_RECEIVE = "time_receive";
    public static final String COLUMN_TIME_SEND = "time_send";
    public static final String COLUMN_TIME_COMPLETE = "time_complete";
    public static final String COLUMN_SMS_FILTER = "sms_filter";
    public static final String COLUMN_SMS_RX_FROM_PHONE = "sms_rx_from_phone";

    public String login;

    public long commandId;

    public String commandType;

    public boolean commandWiSMS;

    public String command;

    public String commandData;

    public String status = null;

    public String smsRxFromPhone;

    public String smsFilter;

    public String answer = "";

    public String ussd = "";

    public Date timeReceive;

    public Date timeSend;

    public Date timeComplete;

    public Command() {}

    @Override
    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        if (id != NONE) {
            values.put(COLUMN_ID, id);
        }
        values.put(COLUMN_LOGIN, login);
        values.put(COLUMN_COMMAND_ID, commandId);
        values.put(COLUMN_COMMAND_TYPE, commandType);
        values.put(COLUMN_COMMAND_WI_SMS, commandWiSMS ? 1 : 0);
        values.put(COLUMN_COMMAND, command);
        values.put(COLUMN_COMMAND_DATA, commandData);
        values.put(COLUMN_STATUS, status);
        values.put(COLUMN_ANSWER, answer);
        values.put(COLUMN_TIME_RECEIVE, fromDateTime(timeReceive));
        values.put(COLUMN_TIME_SEND, fromDateTime(timeSend));
        values.put(COLUMN_TIME_COMPLETE, fromDateTime(timeComplete));
        values.put(COLUMN_SMS_FILTER, smsFilter);
        values.put(COLUMN_SMS_RX_FROM_PHONE, smsRxFromPhone);
        return values;
    }

    @Override
    public void parseCursor(Cursor cursor) {
        id = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_ID));
        login = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_LOGIN));
        commandId = cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_COMMAND_ID));
        commandType = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_COMMAND_TYPE));
        commandWiSMS = cursor.getInt(cursor.getColumnIndexOrThrow(COLUMN_COMMAND_WI_SMS)) == 1;
        command = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_COMMAND));
        commandData = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_COMMAND_DATA));
        status = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_STATUS));
        answer = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_ANSWER));
        timeReceive = toDateTime(cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_TIME_RECEIVE)));
        timeSend = toDateTime(cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_TIME_SEND)));
        timeComplete = toDateTime(cursor.getLong(cursor.getColumnIndexOrThrow(COLUMN_TIME_COMPLETE)));
        smsFilter = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_SMS_FILTER));
        smsRxFromPhone = cursor.getString(cursor.getColumnIndexOrThrow(COLUMN_SMS_RX_FROM_PHONE));
    }

    @Nullable
    private Long fromDateTime(Date dateTime) {
        return dateTime == null ? null : dateTime.getTime();
    }

    @Nullable
    private Date toDateTime(Long dateTime) {
        return dateTime == null ? null : new Date(dateTime);
    }

    @Override
    public String getTable() {
        return TABLE;
    }

    @Override
    public String toString() {
        return "Command{" +
            "login='" + login + '\'' +
            ", commandId=" + commandId +
            ", commandType='" + commandType + '\'' +
            ", commandWiSMS=" + commandWiSMS +
            ", command='" + command + '\'' +
            ", commandData='" + commandData + '\'' +
            ", status='" + status + '\'' +
            ", smsRxFromPhone='" + smsRxFromPhone + '\'' +
            ", smsFilter='" + smsFilter + '\'' +
            ", answer='" + answer + '\'' +
            ", ussd='" + ussd + '\'' +
            ", timeReceive=" + timeReceive +
            ", timeSend=" + timeSend +
            ", timeComplete=" + timeComplete +
            ", id=" + id +
            '}';
    }
}