package rf.androidovshchik.ussdsender.models.remote;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "data")
public class Data {

    public static final String SEPARATOR = "/**/";

    @Element
    public String answer = "";
}
