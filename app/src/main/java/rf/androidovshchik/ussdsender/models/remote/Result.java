package rf.androidovshchik.ussdsender.models.remote;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "result")
public class Result {

    @Element(name = "command_id")
    public long commandId;

    @Element
    public String status = "ok";

    @Element
    public Data data;

    public Result(long commandId, String answer) {
        this.commandId = commandId;
        data = new Data();
        data.answer = answer;
    }
}
