package rf.androidovshchik.ussdsender.models;

import android.content.ContentValues;
import android.database.Cursor;

public abstract class Row {

	// id
	public static final int NONE = -1;

	public static final String COLUMN_ID = "id";

	public long id = NONE;

	public abstract ContentValues toContentValues();

	public abstract void parseCursor(Cursor cursor);

	public abstract String getTable();
}
