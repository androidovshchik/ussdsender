package rf.androidovshchik.ussdsender.models.remote;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

@Root(name = "request")
public class Request {

    @Element
    public String type;

    @Element
    public String login;

    public String password;

    @Element
    public String imei;

    @Element
    public String sign;
}
