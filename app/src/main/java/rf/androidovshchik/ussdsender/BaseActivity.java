package rf.androidovshchik.ussdsender;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import io.reactivex.disposables.CompositeDisposable;
import pub.devrel.easypermissions.EasyPermissions;
import rf.androidovshchik.ussdsender.data.Prefs;
import rf.androidovshchik.ussdsender.receivers.AlarmUtil;
import rf.androidovshchik.ussdsender.services.BaseCallback;
import timber.log.Timber;

public class BaseActivity extends Activity {

    private static final int RC_ALL = 20;

    protected Prefs prefs;

    protected Serializer serializer = new Persister();

    protected CompositeDisposable disposable = new CompositeDisposable();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Timber.d(getClass().getSimpleName());
        prefs = new Prefs(getApplicationContext());
        if (!prefs.has(Prefs.LOGIN)) {
            if (switchSafelyActivity(LoginActivity.class)) {
                return;
            }
        } else {
            if (switchSafelyActivity(MainActivity.class)) {
                return;
            }
        }
        if (getClass() != BaseActivity.class) {
            if (!EasyPermissions.hasPermissions(this, Manifest.permission.CALL_PHONE,
                Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE)) {
                requestPermissions(RC_ALL, Manifest.permission.CALL_PHONE,
                    Manifest.permission.SEND_SMS, Manifest.permission.READ_PHONE_STATE);
            }
        }
    }

    protected boolean switchSafelyActivity(Class activity) {
        if (!getClass().equals(activity)) {
            Intent intent = new Intent(getApplicationContext(), activity);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
            return true;
        }
        return false;
    }

    protected void requestPermissions(int requestCode, String... permissions) {
        EasyPermissions.requestPermissions(this, "Необходимо предоставить требуемые разрешения для работы приложения",
            requestCode, permissions);
    }

    protected void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT)
            .show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_base, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                return true;
            case R.id.action_logout:
                AlarmUtil.cancelAlarm(getApplicationContext());
                prefs.remove(Prefs.LOGIN);
                prefs.remove(Prefs.PASSWORD);
                switchSafelyActivity(LoginActivity.class);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    public abstract class BaseUICallback extends BaseCallback {

        @Override
        public void showMessage(String message) {
            runOnUiThread(() -> BaseActivity.this.showMessage(message));
        }

        @Override
        public boolean handleCallback() {
            return !isFinishing();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
