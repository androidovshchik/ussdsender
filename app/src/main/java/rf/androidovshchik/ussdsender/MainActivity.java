package rf.androidovshchik.ussdsender;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import rf.androidovshchik.ussdsender.data.DbManager;
import rf.androidovshchik.ussdsender.data.DbUtil;
import rf.androidovshchik.ussdsender.data.Prefs;
import rf.androidovshchik.ussdsender.models.Command;
import rf.androidovshchik.ussdsender.services.MainService;
import rf.androidovshchik.ussdsender.services.ServiceUtil;
import rf.androidovshchik.ussdsender.services.USSDService;
import timber.log.Timber;

public class MainActivity extends BaseActivity {

    public static final int LIMIT = 100;

    @BindView(R.id.swipe_container)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.load_more)
    Button loadMore;

    private CommandsAdapter adapter;

    private DbManager manager;

    private String login;

    private long minCommandId = 0;

    private String filter = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setTitle("Главная");
        findViewById(R.id.layout).requestFocus();
        swipeRefreshLayout.setOnRefreshListener(() -> {
            minCommandId = 0;
            loadMore.setVisibility(View.GONE);
            load(null);
        });
        RecyclerView recyclerView = findViewById(R.id.output);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
            DividerItemDecoration.VERTICAL));
        adapter = new CommandsAdapter();
        recyclerView.setAdapter(adapter);
        manager = new DbManager(getApplicationContext());
        login = prefs.getString(Prefs.LOGIN);
        if (!ServiceUtil.isRunning(getApplicationContext(), MainService.class) && prefs.has(Prefs.WORK)) {
            startServiceRightWay(getApplicationContext(),
                ServiceUtil.getIntent(getApplicationContext(), MainService.class));
        }
        load(null);
    }

    private void startServiceRightWay(Context context, Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent);
        } else {
            context.startService(intent);
        }
    }

    @OnTextChanged(value = R.id.filter, callback = OnTextChanged.Callback.AFTER_TEXT_CHANGED)
    void afterInput(Editable editable) {
        filter = editable.toString().toLowerCase().trim();
        Timber.d("filter: " + filter);
        minCommandId = 0;
        loadMore.setVisibility(View.GONE);
        load(null);
    }

    private void openAppSettings() {
        AlertDialog alertDialog = new AlertDialog.Builder(this)
            .create();
        alertDialog.setMessage(getString(R.string.accessibility_service_description));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
            getString(android.R.string.ok), (DialogInterface dialog, int id) -> {
                Intent intent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            });
        alertDialog.show();
    }

    public void load(View view) {
        if (view == null) {
            adapter.commands.clear();
            adapter.notifyDataSetChanged();
        } else {
            loadMore.setVisibility(View.GONE);
        }
        disposable.clear();
        disposable.add(Observable.fromCallable(() -> {
            String query = "SELECT * FROM commands WHERE login = '" + login + "'";
            if (!filter.isEmpty()) {
                query += " AND CAST(command_id AS TEXT) LIKE '%" + filter + "%'";
            }
            if (minCommandId != 0) {
                query += " AND id < " + minCommandId;
            }
            query += " ORDER BY id DESC LIMIT " + LIMIT;
            Timber.d(query);
            ArrayList<Command> commands = DbUtil.getRows(manager.db.query(query), Command.class);
            if (commands.size() >= 1) {
                if (commands.size() >= 2) {
                    Timber.d("Start with id = %d End with id = %d", commands.get(0).id,
                        commands.get(commands.size() - 1).id);
                } else {
                    Timber.d("One with id = %d", commands.get(0).id);
                }
            } else {
                Timber.d("No id");
            }
            adapter.commands.addAll(commands);
            return commands.size() >= LIMIT;
        }).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe((Boolean upToLimitSize) -> {
                if (swipeRefreshLayout.isRefreshing()) {
                    swipeRefreshLayout.setRefreshing(false);
                }
                if (adapter.commands.size() > 0) {
                    minCommandId = adapter.commands.get(adapter.commands.size() - 1).id;
                }
                loadMore.setVisibility(upToLimitSize ? View.VISIBLE : View.GONE);
                adapter.notifyDataSetChanged();
            }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    @SuppressWarnings("all")
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.controller:
                if (!USSDUtil.isAccessibilitySettingsOn(getApplicationContext())) {
                    openAppSettings();
                    return true;
                }
                boolean isRunning = ServiceUtil.isRunning(getApplicationContext(), MainService.class);
                if (isRunning) {
                    prefs.remove(Prefs.WORK);
                    Toast.makeText(getApplicationContext(), "Подождите...", Toast.LENGTH_SHORT)
                        .show();
                } else {
                    prefs.putBoolean(Prefs.WORK, true);
                    startServiceRightWay(getApplicationContext(),
                        ServiceUtil.getIntent(getApplicationContext(), MainService.class));
                }
                invalidateOptionsMenu();
                return true;
            case R.id.action_ussd_logs:
                if (!USSDUtil.isAccessibilitySettingsOn(getApplicationContext())) {
                    openAppSettings();
                    return true;
                }
                Toast.makeText(getApplicationContext(), "Подождите...",
                    Toast.LENGTH_SHORT).show();
                StringBuilder ussdLogs = new StringBuilder();
                for (int i = 0; i < USSDService.USSD_LOGS.size(); i++) {
                    ussdLogs.append(USSDService.USSD_LOGS.get(i));
                    ussdLogs.append("\n");
                }
                sendEmail("Логи окон", ussdLogs.toString());
                return true;
            case R.id.action_logcat:
                swipeRefreshLayout.setEnabled(false);
                Toast.makeText(getApplicationContext(), "Подождите...",
                    Toast.LENGTH_SHORT).show();
                disposable.add(Observable.fromCallable(() -> {
                    StringBuilder appLogs = new StringBuilder();
                    try {
                        Process process = Runtime.getRuntime().exec("logcat -d");
                        BufferedReader bufferedReader =
                            new BufferedReader(new InputStreamReader(process.getInputStream()));
                        String line;
                        while ((line = bufferedReader.readLine()) != null) {
                            appLogs.append(line);
                            appLogs.append("\n");
                        }
                    } catch (Exception e) {
                        Timber.e(e);
                        return "";
                    }
                    return appLogs.toString();
                }).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe((String appLogs) -> {
                        swipeRefreshLayout.setEnabled(true);
                        sendEmail("Логи приложения", appLogs);
                    }));
                return true;
            case R.id.action_version:
                AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setPositiveButton(android.R.string.ok, null)
                    .create();
                alertDialog.setMessage("Версия: " + BuildConfig.VERSION_NAME);
                alertDialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sendEmail(String title, String logs) {
        if (logs.trim().isEmpty()) {
            Toast.makeText(getApplicationContext(), "Не удалось получить логи",
                Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{});
        intent.putExtra(Intent.EXTRA_SUBJECT, title);
        intent.putExtra(Intent.EXTRA_TEXT, logs);
        try {
            startActivity(Intent.createChooser(intent, "Отправить письмо..."));
        } catch (ActivityNotFoundException e) {
            Timber.e(e);
            Toast.makeText(getApplicationContext(), "Не удалось отправить письмо",
                Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean isServiceRunning = ServiceUtil.isRunning(getApplicationContext(), MainService.class);
        menu.findItem(R.id.controller).setTitle(isServiceRunning ? "Остановить" : "Запустить");
        return super.onPrepareOptionsMenu(menu);
    }
}
