package rf.androidovshchik.ussdsender;

import android.content.Context;
import android.provider.Settings;
import android.text.TextUtils;

import rf.androidovshchik.ussdsender.services.USSDService;
import timber.log.Timber;

public class USSDUtil {

    public static boolean isAccessibilitySettingsOn(Context context) {
        int accessibilityEnabled = 0;
        String service = context.getPackageName() + "/" + USSDService.class.getCanonicalName();
        try {
            accessibilityEnabled = Settings.Secure.getInt(context.getContentResolver(),
                Settings.Secure.ACCESSIBILITY_ENABLED);
        } catch (Settings.SettingNotFoundException e) {
            Timber.e(e);
        }
        TextUtils.SimpleStringSplitter stringColonSplitter =
            new TextUtils.SimpleStringSplitter(':');
        if (accessibilityEnabled == 1) {
            String settingValue = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            if (settingValue != null) {
                stringColonSplitter.setString(settingValue);
                while (stringColonSplitter.hasNext()) {
                    String accessibilityService = stringColonSplitter.next();
                    if (accessibilityService.equalsIgnoreCase(service)) {
                        Timber.d("The correct accessibility is switched on!");
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
