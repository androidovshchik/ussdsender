package rf.androidovshchik.ussdsender;

import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;
import rf.androidovshchik.ussdsender.data.Prefs;
import rf.androidovshchik.ussdsender.models.remote.Request;
import rf.androidovshchik.ussdsender.services.MainService;
import rf.androidovshchik.ussdsender.services.RequestUtil;
import rf.androidovshchik.ussdsender.services.ServiceUtil;

public class LoginActivity extends BaseActivity {

    private static final int RC_PHONE = 10;

    private EditText loginView;

    private EditText passwordView;

    private TelephonyManager telephonyManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle("Регистрация");
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        findViewById(R.id.layout).requestFocus();
        loginView = findViewById(R.id.login);
        passwordView = findViewById(R.id.password);
        if (ServiceUtil.isRunning(getApplicationContext(), MainService.class)) {
            stopService(ServiceUtil.getIntent(getApplicationContext(), MainService.class));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @AfterPermissionGranted(RC_PHONE)
    @SuppressWarnings("unused")
    public void login() {
        login(null);
    }

    @SuppressWarnings("all")
    public void login(View view) {
        if (!EasyPermissions.hasPermissions(this, Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_PHONE_STATE)) {
            requestPermissions(RC_PHONE, Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE);
            return;
        }
        Request request = new Request();
        request.type = "login";
        request.login = loginView.getText().toString().trim();
        if (request.login.isEmpty()) {
            showMessage("Не задан логин");
            return;
        }
        request.password = passwordView.getText().toString().trim();
        if (request.password.isEmpty()) {
            showMessage("Не задан пароль");
            return;
        }
        if (!prefs.has(Prefs.URL)) {
            showMessage("Не задана ссылка для регистрации");
            return;
        }
        request.imei = telephonyManager.getDeviceId();
        request.sign = RequestUtil.getMd5Hash(request.login + request.password +
            request.type + request.imei);
        RequestUtil.makeCall(prefs.getString(Prefs.URL), serializer, request, new BaseUICallback() {

            boolean isLogged = false;

            @Override
            public  void onText(String text) {
                switch (tag) {
                    case "message":
                        runOnUiThread(() -> showMessage(text));
                        break;
                    case "status":
                        isLogged = text.trim().toLowerCase().equals("success");
                        break;
                }
            }

            @Override
            public void onFinish() {
                if (isLogged) {
                    prefs.putString(Prefs.LOGIN, request.login);
                    prefs.putString(Prefs.PASSWORD, request.password);
                    switchSafelyActivity(MainActivity.class);
                }
            }
        });
    }
}
