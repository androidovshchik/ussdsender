<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-Type, Accept');
header('Access-Control-Allow-Methods: POST');
header('Content-Type: application/xml; charset=utf-8');

$dataPOST = trim(file_get_contents('php://input'));
$xmlData = simplexml_load_string($dataPOST);

// Разного рода дополнительных проверок здесь нет
// приложение рассчитывает на данные респонзы, если будут другие
// они будут игнорироваться

$imei = $xmlData->imei;
switch ($xmlData->type) {
	case "login":
		if ($xmlData->login == 'android' && md5($xmlData->login . 'lollipop') == $xmlData->sign) {
			echo <<<XML
<?xml version='1.0'?>
<response>
<status>success</status>
<message>вы успешно зарегистрировались</message>
<sign>$xmlData->sign</sign>
</response>
XML;
		} else {
			echo <<<XML
<?xml version='1.0'?>
<response>
<status>error</status>
<message>неверный логин и пароль</message>
<sign>$xmlData->sign</sign>
</response>
XML;
		}
		break;
	case "command":
		$commandId = rand(1, 1000);
		switch(rand(1, 4)) {
			case 1:
    			echo <<<XML
<?xml version='1.0'?>
<response>
<command_id>$commandId</command_id>
<command_type>ussd</command_type>
<command>*100#</command>
<command_data></command_data>
<sign>$xmlData->sign</sign>
</response>
XML;
				break;
			case 2:
    			echo <<<XML
<?xml version='1.0'?>
<response>
<command_id>$commandId</command_id>
<command_type>sms</command_type>
<command>+79999999999</command>
<command_data>Текст сообщения</command_data>
<sign>$xmlData->sign</sign>
</response>
XML;
				break;
			case 3:
    			echo <<<XML
<?xml version='1.0'?>
<response>
<command_id>$commandId</command_id>
<command_type>phone</command_type>
<command>+79999999999</command>
<command_data></command_data>
<sign>$xmlData->sign</sign>
</response>
XML;
				break;
			case 4:
    			echo <<<XML
<?xml version='1.0'?>
<response>
<sign>$xmlData->sign</sign>
</response>
XML;
				break;
		}
		break;
	case "result":
		$commandId = $xmlData->result->command_id;
		switch(rand(1, 2)) {
			case 1:
    			echo <<<XML
<?xml version='1.0'?>
<response>
<command_id>$commandId</command_id>
<command_status>accepted</command_status>
<sign>$xmlData->sign</sign>
</response>
XML;
				break;
			case 2:
    			echo <<<XML
<?xml version='1.0'?>
<response>
<command_id>$commandId</command_id>
<command_status>rejected</command_status>
<sign>$xmlData->sign</sign>
</response>
XML;
				break;
		}
		break;
}